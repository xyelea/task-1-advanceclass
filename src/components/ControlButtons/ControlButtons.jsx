import React from "react";
import "./ControlButtons.css";
const ControlButtons = ({ isRunning, onStart, onStop, onReset }) => {
  return (
    <div className=" btn-grp ">
      <button onClick={onStart} className="btn-1 btn-one">
        Start
      </button>
      <button onClick={onStop} className="btn-2 btn-two">
        Stop
      </button>
      <button onClick={onReset} className="btn-3 btn-three">
        Reset
      </button>
    </div>
  );
};

export default ControlButtons;
