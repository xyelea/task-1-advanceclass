import React from "react";
import "./Timer.css";

const Timer = ({ time }) => {
  const formatTime = (time) => {
    const hours = Math.floor(time / 3600);
    const minutes = Math.floor((time % 3600) / 60);
    const seconds = time % 60;

    return `${("0" + hours).slice(-2)}:${("0" + minutes).slice(-2)}:${(
      "0" + seconds
    ).slice(-2)}`;
  };

  return <div className="timer digits">{formatTime(time)}</div>;
};

export default Timer;
